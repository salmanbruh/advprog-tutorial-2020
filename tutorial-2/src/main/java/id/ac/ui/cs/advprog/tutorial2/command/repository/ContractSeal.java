package id.ac.ui.cs.advprog.tutorial2.command.repository;

import id.ac.ui.cs.advprog.tutorial2.command.core.spell.Spell;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Repository
public class ContractSeal {

    private Map<String, Spell> spells;
    boolean lastUndo = false;
    Spell latestSpell = null;

    public ContractSeal() {
        spells = new HashMap<>();
    }

    public void registerSpell(Spell spell) {
        spells.put(spell.spellName(), spell);
    }

    public void castSpell(String spellName) {
        // TODO: Complete Me
        Spell spell = spells.get(spellName);
        spell.cast();
        latestSpell = spell;
        lastUndo = false;
    }

    public void undoSpell() {
        // TODO: Complete Me
        if (!lastUndo || !(latestSpell == null)){
            latestSpell.undo();
            lastUndo = true;
        }
    }

    public Collection<Spell> getSpells() { return spells.values(); }
}
