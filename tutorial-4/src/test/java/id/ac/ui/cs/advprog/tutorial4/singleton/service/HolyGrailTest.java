package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.HolyWish;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class HolyGrailTest {

    // TODO create tests
    @Mock
    private HolyWish holyWish;

    @InjectMocks
    private HolyGrail holyGrail;

    @Test
    public void whenMakeAWishIsCalledItShouldCallHolyWishSetName(){
        String wishName = "Makan enak";

        holyGrail.makeAWish(wishName);

        verify(holyWish, times(1)).setWish(wishName);
    }

    @Test
    public void whenGetHolyWishIsCalledItShouldReturnHolyWishObject(){
        HolyWish holyWish = holyGrail.getHolyWish();

        assertEquals(holyWish, holyGrail.getHolyWish());
    }
}
