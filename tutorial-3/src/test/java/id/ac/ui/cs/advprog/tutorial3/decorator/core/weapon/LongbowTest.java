package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LongbowTest {


    private Weapon weapon;

    @BeforeEach
    public void setUp(){
        weapon = new Longbow();
    }

    @Test
    public void testMethodGetWeaponName(){
        //TODO: Complete me
        String weapName = weapon.getName();
        assertEquals("Big Longbow", weapName);
    }

    @Test
    public void testMethodGetWeaponDescription(){
        //TODO: Complete me
        String weapDesc = weapon.getDescription();
        assertEquals("Ranged weapon that shoots arrow.", weapDesc);
    }

    @Test
    public void testMethodGetWeaponValue(){
        //TODO: Complete me
        int weapVal = weapon.getWeaponValue();
        assertEquals(15, weapVal);
    }
}
