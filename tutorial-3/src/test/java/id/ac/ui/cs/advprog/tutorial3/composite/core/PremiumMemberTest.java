package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName()
    {
        //TODO: Complete me
        String name = member.getName();
        assertEquals("Aqua", name);
    }

    @Test
    public void testMethodGetRole() {
        //TODO: Complete me
        String role = member.getRole();
        assertEquals("Goddess", role);
    }

    @Test
    public void testMethodAddChildMember() {
        //TODO: Complete me
        Member child = new OrdinaryMember("Ord", "Mem");
        member.addChildMember(child);
        assertEquals(1, member.getChildMembers().size());
    }

    @Test
    public void testMethodRemoveChildMember() {
        //TODO: Complete me
        Member child = new OrdinaryMember("Ord", "Mem");
        member.addChildMember(child);
        member.removeChildMember(child);
        assertEquals(0, member.getChildMembers().size());
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member child1 = new OrdinaryMember("Ord", "Mem");
        Member child2 = new OrdinaryMember("Ord", "Mem");
        Member child3 = new OrdinaryMember("Ord", "Mem");
        Member child4 = new OrdinaryMember("Ord", "Mem");

        member.addChildMember(child1);
        member.addChildMember(child2);
        member.addChildMember(child3);
        member.addChildMember(child4);

        assertEquals(3, member.getChildMembers().size());
    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member master = new PremiumMember("Game Master", "Master");
        Member child1 = new OrdinaryMember("Ord", "Mem");
        Member child2 = new OrdinaryMember("Ord", "Mem");
        Member child3 = new OrdinaryMember("Ord", "Mem");
        Member child4 = new OrdinaryMember("Ord", "Mem");

        master.addChildMember(child1);
        master.addChildMember(child2);
        master.addChildMember(child3);
        master.addChildMember(child4);

        assertEquals(4, master.getChildMembers().size());
    }
}
