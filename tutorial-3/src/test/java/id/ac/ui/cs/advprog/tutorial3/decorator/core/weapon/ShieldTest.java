package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ShieldTest {

    private Weapon weapon;

    @BeforeEach
    public void setUp(){
        weapon = new Shield();
    }

    @Test
    public void testMethodGetWeaponName(){
        //TODO: Complete me
        String weapName = weapon.getName();
        assertEquals("Heater Shield", weapName);
    }

    @Test
    public void testMethodGetWeaponDescription(){
        //TODO: Complete me
        String weapDesc = weapon.getDescription();
        assertEquals("Weapon that deflects attacks.", weapDesc);
    }

    @Test
    public void testMethodGetWeaponValue(){
        //TODO: Complete me
        int weapVal = weapon.getWeaponValue();
        assertEquals(10, weapVal);
    }
}
