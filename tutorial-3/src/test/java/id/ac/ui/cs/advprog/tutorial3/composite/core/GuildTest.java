package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.assertj.core.api.Assertions.assertThat;

public class GuildTest {
    private Guild guild;
    private Member guildMaster;

    @BeforeEach
    public void setUp() {
        guildMaster = new PremiumMember("Eko", "Master");
        guild = new Guild(guildMaster);
    }

    @Test
    public void testMethodAddMember() {
        //TODO: Complete me
        Member child = new OrdinaryMember("Ord", "Leaf");
        guild.addMember(guildMaster, child);

        List<Member> guildMasterChild = guildMaster.getChildMembers();
        assertThat(guildMasterChild).hasSize(1);
        assertThat(guildMasterChild).contains(child);

        List<Member> guildMemberList = guild.getMemberList();
        assertThat(guildMemberList).hasSize(2);
        assertThat(guildMemberList).contains(child);
    }

    @Test
    public void testMethodRemoveMember() {
        //TODO: Complete me
        Member memberToRemove = new OrdinaryMember("Ord", "Leaf");
        guild.addMember(guildMaster, memberToRemove);

        guild.removeMember(guildMaster, memberToRemove);

        List<Member> guildMasterChild = guildMaster.getChildMembers();
        assertThat(guildMasterChild).hasSize(0);
        assertThat(guildMasterChild).doesNotContain(memberToRemove);

        List<Member> guildMemberList = guild.getMemberList();
        assertThat(guildMemberList).hasSize(1);
        assertThat(guildMemberList).doesNotContain(memberToRemove);
    }

    @Test
    public void testMethodMemberHierarchy() {
        Member sasuke = new OrdinaryMember("Asep", "Servant");
        guild.addMember(guildMaster, sasuke);
        assertEquals(guildMaster, guild.getMemberHierarchy().get(0));
        assertEquals(sasuke, guild.getMemberHierarchy().get(0).getChildMembers().get(0));
    }

    @Test
    public void testMethodGetMember() {
        //TODO: Complete me
        Member testMem = new OrdinaryMember("Test", "Leaf");

        Member testGet = guild.getMember("Test", "Leaf");
        assertThat(testGet).isNull();

        guild.addMember(guildMaster, testMem);
        testGet = guild.getMember("Test", "Leaf");
        assertThat(testGet).isNotNull();
    }
}
