package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class OrdinaryMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new OrdinaryMember("Nina", "Merchant");
    }

    @Test
    public void testMethodGetName() {
        //TODO: Complete me
        String name = member.getName();
        assertEquals("Asuna", name);
    }

    @Test
    public void testMethodGetRole() {
        //TODO: Complete me
        String role = member.getRole();
        assertEquals("Waifu", role);
    }

    @Test
    public void testMethodAddRemoveChildMemberDoNothing() {
        //TODO: Complete me
        Member tempChild = new OrdinaryMember("Temp","Child");

        member.addChildMember(tempChild);
        assertEquals(0, member.getChildMembers().size());

        member.removeChildMember(tempChild);
        assertEquals(0, member.getChildMembers().size());
    }
}
