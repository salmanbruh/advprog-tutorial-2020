package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SwordTest {

    private Weapon weapon;

    @BeforeEach
    public void setUp(){
        weapon = new Sword();
    }

    @Test
    public void testMethodGetWeaponName(){
        //TODO: Complete me
        String weapName = weapon.getName();
        assertEquals("Great Sword", weapName);
    }

    @Test
    public void testMethodGetWeaponDescription(){
        //TODO: Complete me
        String weapDesc = weapon.getDescription();
        assertEquals("Melee Weapon.", weapDesc);
    }

    @Test
    public void testMethodGetWeaponValue(){
        //TODO: Complete me
        int weapVal = weapon.getWeaponValue();
        assertEquals(25, weapVal);
    }
}
