package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Longbow;
import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MagicUpgradeTest {


    private MagicUpgrade magicUpgrade;

    @BeforeEach
    public void setUp(){
        magicUpgrade = new MagicUpgrade(new Longbow());
    }

    @Test
    public void testMethodGetWeaponName(){
        //TODO: Complete me
        String weapName = magicUpgrade.getName();
        assertEquals("Big Longbow", weapName);
    }

    @Test
    public void testMethodGetWeaponDescription(){
        //TODO: Complete me
        String weapDesc = magicUpgrade.getDescription();
        assertEquals("Ranged weapon that shoots arrow. Enhanced with Magic.", weapDesc);
    }

    @Test
    public void testMethodGetWeaponValue(){
        //TODO: Complete me
        Weapon weap = magicUpgrade.weapon;
        int defaultVal = weap.getWeaponValue();
        int upgradedVal = magicUpgrade.getWeaponValue();
        assertTrue(upgradedVal >= defaultVal + 15 && upgradedVal <= defaultVal + 20);
    }
}
