package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Shield;
import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RawUpgradeTest {

    private RawUpgrade rawUpgrade;

    @BeforeEach
    public void setUp(){
        rawUpgrade = new RawUpgrade(new Shield());
    }

    @Test
    public void testMethodGetWeaponName(){
        //TODO: Complete me
        String weapName = rawUpgrade.getName();
        assertEquals("Heater Shield", weapName);
    }

    @Test
    public void testMethodGetDescription(){
        //TODO: Complete me
        String weapDesc = rawUpgrade.getDescription();
        assertEquals("Weapon that deflects attacks. Enhanced with Raw.", weapDesc);
    }

    @Test
    public void testMethodGetWeaponValue(){
        //TODO: Complete me
        Weapon weap = rawUpgrade.weapon;
        int defaultVal = weap.getWeaponValue();
        int upgradedVal = rawUpgrade.getWeaponValue();
        assertTrue(upgradedVal >= defaultVal + 5 && upgradedVal <= defaultVal + 10);
    }
}
