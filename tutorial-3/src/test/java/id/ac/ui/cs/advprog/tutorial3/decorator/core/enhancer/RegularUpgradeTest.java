package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Sword;
import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RegularUpgradeTest {

    private RegularUpgrade regularUpgrade;

    @BeforeEach
    public void setUp(){
        regularUpgrade = new RegularUpgrade(new Sword());
    }

    @Test
    public void testMethodGetWeaponName(){
        //TODO: Complete me
        String weapName = regularUpgrade.getName();
        assertEquals("Great Sword", weapName);
    }

    @Test
    public void testMethodGetWeaponDescription(){
        //TODO: Complete me
        String weapDesc = regularUpgrade.getDescription();
        assertEquals("Melee Weapon. Enhanced with Regular.", weapDesc);
    }

    @Test
    public void testMethodGetWeaponValue(){
        //TODO: Complete me
        Weapon weap = regularUpgrade.weapon;
        int defaultVal = weap.getWeaponValue();
        int upgradedVal = regularUpgrade.getWeaponValue();
        assertTrue(upgradedVal >= defaultVal + 1 && upgradedVal <= defaultVal + 5);
    }
}
