package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Shield;
import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UniqueUpgradeTest {

    private UniqueUpgrade uniqueUpgrade;

    @BeforeEach
    public void setUp(){
        uniqueUpgrade = new UniqueUpgrade(new Shield());
    }

    @Test
    public void testMethodGetWeaponName(){
        //TODO: Complete me
        String weapName = uniqueUpgrade.getName();
        assertEquals("Heater Shield", weapName);
    }

    @Test
    public void testMethodGetWeaponDescription(){
        //TODO: Complete me
        String weapDesc = uniqueUpgrade.getDescription();
        assertEquals("Weapon that deflects attacks. Enhanced with Unique.", weapDesc);
    }

    @Test
    public void testMethodGetWeaponValue(){
        //TODO: Complete me
        Weapon weap = uniqueUpgrade.weapon;
        int defaultVal = weap.getWeaponValue();
        int upgradedVal = uniqueUpgrade.getWeaponValue();
        assertTrue(upgradedVal >= defaultVal + 10 && upgradedVal <= defaultVal + 15);
    }
}
