package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;


import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Gun;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class ChaosUpgradeTest {

    private ChaosUpgrade chaosUpgrade;
    @BeforeEach
    public void setUp(){
        chaosUpgrade = new ChaosUpgrade(new Gun());
    }

    @Test
    public void testMethodGetWeaponName(){
        //TODO: Complete me
        String weapName = chaosUpgrade.getName();
        assertEquals("Automatic Gun", weapName);
    }

    @Test
    public void testGetMethodWeaponDescription(){
        //TODO: Complete me
        String weapDesc = chaosUpgrade.getDescription();
        assertEquals("Ranged weapon that shoots ammunition. Enhanced with Chaos.", weapDesc);
    }

    @Test
    public void testMethodGetWeaponValue(){
        //TODO: Complete me
        int defaultVal = chaosUpgrade.weapon.getWeaponValue();
        int upgradedVal = chaosUpgrade.getWeaponValue();
        assertTrue(upgradedVal >= defaultVal + 50 && upgradedVal <= defaultVal + 55);
    }
}
