package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public class Longbow extends Weapon {
        //TODO: Complete me
    public Longbow(){
        this.weaponName = "Big Longbow";
        this.weaponDescription = "Ranged weapon that shoots arrow.";
        this.weaponValue = 15;
    }
}
