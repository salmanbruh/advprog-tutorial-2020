package id.ac.ui.cs.advprog.tutorial3.decorator.repository;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer.EnhancerDecorator;
import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.WeaponProducer;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public class EnhanceRepository {

    public void enhanceToAllWeapons(ArrayList<Weapon> weapons){

        for (Weapon weapon: weapons) {
            int index = weapons.indexOf(weapon);
            switch (weapon.getName()) {
                case "Automatic Gun":
                    Weapon gun;
                    gun = WeaponProducer.WEAPON_GUN.createWeaponEnhancer();
                    gun = EnhancerDecorator.RAW_UPGRADE.addWeaponEnhancement(gun);
                    gun = EnhancerDecorator.REGULAR_UPGRADE.addWeaponEnhancement(gun);
                    weapons.set(index,gun);
                    break;
                //TODO: Complete me
                case "Big Longbow":
                    Weapon longbow;
                    longbow = WeaponProducer.WEAPON_LONGBOW.createWeaponEnhancer();
                    longbow = EnhancerDecorator.MAGIC_UPGRADE.addWeaponEnhancement(longbow);
                    longbow = EnhancerDecorator.REGULAR_UPGRADE.addWeaponEnhancement(longbow);
                    longbow = EnhancerDecorator.UNIQUE_UPGRADE.addWeaponEnhancement(longbow);
                    weapons.set(index,longbow);
                    break;
                case "Heater Shield":
                    Weapon shield;
                    shield = WeaponProducer.WEAPON_SHIELD.createWeaponEnhancer();
                    shield = EnhancerDecorator.RAW_UPGRADE.addWeaponEnhancement(shield);
                    shield = EnhancerDecorator.MAGIC_UPGRADE.addWeaponEnhancement(shield);
                    weapons.set(index,shield);
                    break;
                case "Great Sword":
                    Weapon sword;
                    sword = WeaponProducer.WEAPON_SWORD.createWeaponEnhancer();
                    sword = EnhancerDecorator.UNIQUE_UPGRADE.addWeaponEnhancement(sword);
                    sword = EnhancerDecorator.MAGIC_UPGRADE.addWeaponEnhancement(sword);
                    weapons.set(index,sword);
                    break;
            }
        }
    }
}
