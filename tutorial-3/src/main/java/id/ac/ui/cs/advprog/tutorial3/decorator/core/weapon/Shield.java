package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public class Shield extends Weapon {
        //TODO: Complete me
    public Shield(){
        this.weaponName = "Heater Shield";
        this.weaponDescription = "Weapon that deflects attacks.";
        this.weaponValue = 10;
    }
}
