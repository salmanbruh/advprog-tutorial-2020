package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public class Gun extends Weapon {
        //TODO: Complete me
    public Gun(){
        this.weaponName = "Automatic Gun";
        this.weaponDescription = "Ranged weapon that shoots ammunition.";
        this.weaponValue = 20;
    }
}
