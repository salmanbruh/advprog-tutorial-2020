package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public class Sword extends Weapon {
        //TODO: Complete me
    public Sword(){
        this.weaponName = "Great Sword";
        this.weaponDescription = "Melee Weapon.";
        this.weaponValue = 25;
    }
}
