package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class MagicUpgrade extends Weapon {

    Weapon weapon;

    public MagicUpgrade(Weapon weapon) {

        this.weapon= weapon;
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 15-20 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        int randomVal = new Random().nextInt(5+1) + 15;
        int prevVal = weapon.getWeaponValue();
        int upgradedVal = prevVal + randomVal;
        return upgradedVal;
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        return weapon.getDescription() + " Enhanced with Magic.";
    }
}
