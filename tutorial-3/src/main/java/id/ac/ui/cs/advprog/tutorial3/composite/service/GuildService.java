package id.ac.ui.cs.advprog.tutorial3.composite.service;

import id.ac.ui.cs.advprog.tutorial3.composite.core.Member;

import java.util.List;

public interface GuildService {
    public void addMember(String parentNameRole, String childName, String childRole, String childType);

    public void removeMember(String parentNameRole, String childNameRole);

    public List<Member> getMemberHierarchy();

    public List<Member> getMemberList();
}
