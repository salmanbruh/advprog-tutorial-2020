package id.ac.ui.cs.advprog.tutorial5.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "soul")
public class Soul {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "name")
    private String name;

    @Column(name="gender")
    private String gender;

    @Column(name="age")
    private int age;

    @Column(name="occupation")
    private String occupation;

    public long getID(){ return this.id;}

    public String getName(){ return this.name;}

    public void setName(String name){ this.name = name;}

    public String getGender(){ return this.gender;}

    public void setGender(String gender){this.gender = gender;}

    public int getAge(){return this.age;}

    public void setAge(int age){this.age = age;}

    public String getOccupation() {return occupation;}

    public void setOccupation(String occupation){this.occupation = occupation;}

}
