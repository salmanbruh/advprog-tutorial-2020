package id.cs.ui.ac.advprog.tutorial5.service;

import id.ac.ui.cs.advprog.tutorial5.entity.Soul;
import id.ac.ui.cs.advprog.tutorial5.repository.SoulRepository;
import id.ac.ui.cs.advprog.tutorial5.service.SoulServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class SoulServiceImplTest {
    @Mock
    private SoulRepository soulRepository;

    @InjectMocks
    private SoulServiceImpl soulService;

    @Test
    public void whenFindAllIsCalledItShouldCallSoulRepositoryFindAll(){
        Soul soul = new Soul();
        soulService.register(soul);
        assertTrue(soulService.findAll() instanceof List);
    }

    @Test
    public void whenFindSoulIsCalledItShouldCallSoulRepositoryFind(){
        Soul soul = new Soul();
        soulService.register(soul);
        soulService.findSoul(soul.getID());
        verify(soulRepository, times(1)).findById(soul.getID());
    }

    @Test
    public void whenEraseSoulIsCalledItShouldCallSoulRepositoryDelete(){
        Soul soul = new Soul();
        soulService.register(soul);
        soulService.erase(soul.getID());
        verify(soulRepository, times(1)).deleteById(soul.getID());
    }

    @Test
    public void whenRewriteSoulIsCalledItShouldCallSoulRepositorySave(){
        Soul soul = new Soul();
        soulService.register(soul);
        soulService.rewrite(soul);
        verify(soulRepository, times(2)).save(soul);
    }

    @Test
    public void whenRegisterSoulIsCalledItSHouldCallSoulRepositorySave(){
        Soul soul = new Soul();
        soulService.register(soul);
        verify(soulRepository, times(1)).save(soul);
    }
}
