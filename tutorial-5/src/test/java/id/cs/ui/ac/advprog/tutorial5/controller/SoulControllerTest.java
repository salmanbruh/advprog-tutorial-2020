package id.cs.ui.ac.advprog.tutorial5.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.ac.ui.cs.advprog.tutorial5.Tutorial5Application;
import id.ac.ui.cs.advprog.tutorial5.controller.SoulController;
import id.ac.ui.cs.advprog.tutorial5.entity.Soul;
import id.ac.ui.cs.advprog.tutorial5.service.SoulServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = SoulController.class)
@ContextConfiguration(classes = Tutorial5Application.class)
public class SoulControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private SoulServiceImpl soulService;

    @Test
    public void status200WhenCallFindAll() throws Exception {
        mockMvc.perform(get("/soul")
                        .contentType("application/json"))
                        .andExpect(status().isOk());
    }

    @Test
    public void status200WhenCreateSoul() throws Exception {
        Soul soul = new Soul();
        mockMvc.perform(post("/soul/")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(soul)))
                        .andExpect(status().isOk());
    }

    @Test
    public void status200WhenFindById() throws Exception {
    }

    @Test
    public void status200WhenUpdateSoul() throws Exception {
    }

    @Test
    public void status200WhenDeleteSoul() throws Exception {
        Soul soul = new Soul();
        mockMvc.perform(post("/soul/")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(soul)))
                        .andExpect(status().isOk());

        mockMvc.perform(delete("/soul/" + soul.getID())
                        .contentType("application/json"))
                        .andExpect(status().isOk());
    }
}
