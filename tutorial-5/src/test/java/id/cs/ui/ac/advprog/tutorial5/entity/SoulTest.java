package id.cs.ui.ac.advprog.tutorial5.entity;

import id.ac.ui.cs.advprog.tutorial5.entity.Soul;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class SoulTest {
    private Soul soul;

    @BeforeEach
    public void setUp() throws Exception {
        soul = new Soul();
        soul.setAge(15);
        soul.setGender("M");
        soul.setName("Saya");
        soul.setOccupation("Mahasiswa");
    }

    @Test
    public void setNameShouldChangeSoulName() {
        soul.setName("Vincent");
        assertTrue(soul.getName().equals("Vincent"));
    }

    @Test
    public void getNameShouldReturnString(){
        assertTrue(soul.getName() instanceof String);
    }

    @Test
    public void setAgeShouldChangeSoulAge(){
        soul.setAge(12);
        assertEquals(12, soul.getAge());
    }

    @Test
    public void getAgeShouldReturnInt(){
        assertEquals(15, soul.getAge());
    }

    @Test
    public void setGenderShouldChangeSoulGender() {
        soul.setGender("F");
        assertTrue(soul.getGender().equals("F"));
    }

    @Test
    public void getGenderShouldReturnString(){
        assertNotNull(soul.getGender());
    }

    @Test
    public void setOccupationShouldChangeSoulOccupation() {
        soul.setOccupation("Siswa");
        assertTrue(soul.getOccupation().equals("Siswa"));
    }

    @Test
    public void getOccupationShouldReturnString(){
        assertNotNull(soul.getOccupation());
    }
}
