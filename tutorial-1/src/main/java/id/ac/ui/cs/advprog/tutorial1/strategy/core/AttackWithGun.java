package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithGun implements AttackBehavior {
    private String attackType = "Gun";

    @Override
    public String attack() {
        return "Attack with " + attackType + "!!";
    }

    @Override
    public String getType() {
        return attackType;
    }
    //ToDo: Complete me
}
