package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class KnightAdventurer extends Adventurer {
    private String alias = "Knight";

    public KnightAdventurer(){
        setAttackBehavior(new AttackWithSword());
        setDefenseBehavior(new DefendWithArmor());
    }

    @Override
    public String getAlias() {
        return alias;
    }
    //ToDo: Complete me
}
