package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {
    private String defenceType = "Armor";

    @Override
    public String defend() {
        return "Defend with " + defenceType + "!!";
    }

    @Override
    public String getType() {
        return defenceType;
    }
    //ToDo: Complete me
}
