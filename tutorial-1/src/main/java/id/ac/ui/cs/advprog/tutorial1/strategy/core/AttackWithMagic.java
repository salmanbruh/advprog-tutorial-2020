package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {
    private String attackType = "Magic";

    @Override
    public String attack() {
        return "Attack with " + attackType + "!!";
    }

    @Override
    public String getType() {
        return attackType;
    }
    //ToDo: Complete me

}
