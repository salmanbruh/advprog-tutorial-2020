package id.ac.ui.cs.advprog.tutorial1.observer.core;

import java.util.ArrayList;
import java.util.List;

public class Guild {
        private List<Adventurer> adventurers = new ArrayList<>();
        private Quest quest;

        public void add(Adventurer adventurer) {
                adventurers.add(adventurer);
        }

        public void addQuest(Quest quest) {
                this.quest = quest;
                System.out.println("Update Quest : " + this.quest.getTitle() + " " + this.getQuestType()) ;
                broadcast();
        }

        public String getQuestType () {return quest.getType();}

        public Quest getQuest() {return quest;}

        public List<Adventurer> getAdventurers() {
                return adventurers;
        }

        private void broadcast() {
                //ToDo: Complete Me
                for (Adventurer adv : adventurers){
                        System.out.println("Update : " + adv.getName());
                        adv.update();
                }
        }
}
