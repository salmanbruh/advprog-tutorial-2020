package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithSword implements AttackBehavior {
    private String attackType = "Sword";

    @Override
    public String attack() {
        return "Attack with " + attackType + "!!";
    }

    @Override
    public String getType() {
        return attackType;
    }
    //ToDo: Complete me
}
